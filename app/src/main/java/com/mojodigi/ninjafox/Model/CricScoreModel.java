package com.mojodigi.ninjafox.Model;

public class CricScoreModel {


    String matchNote;
    String team1Name;
    String team2Name;
    String team1Score;
    String team2Score;

    public String getTeam1Code() {
        return team1Code;
    }

    public void setTeam1Code(String team1Code) {
        this.team1Code = team1Code;
    }

    public String getTeam2Code() {
        return team2Code;
    }

    public void setTeam2Code(String team2Code) {
        this.team2Code = team2Code;
    }

    String team1Code;
    String team2Code;

    public String getMatchNote() {
        return matchNote;
    }

    public void setMatchNote(String matchNote) {
        this.matchNote = matchNote;
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }

    public String getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(String team1Score) {
        this.team1Score = team1Score;
    }

    public String getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(String team2Score) {
        this.team2Score = team2Score;
    }


}
